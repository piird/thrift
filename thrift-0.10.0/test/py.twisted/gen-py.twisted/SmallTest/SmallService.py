#
# Autogenerated by Thrift Compiler (0.10.0)
#
# DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
#
#  options string: py:twisted
#

from thrift.Thrift import TType, TMessageType, TFrozenDict, TException, TApplicationException
from thrift.protocol.TProtocol import TProtocolException
import sys
import logging
from .ttypes import *
from thrift.Thrift import TProcessor
from thrift.transport import TTransport
from zope.interface import Interface, implements
from twisted.internet import defer
from thrift.transport import TTwisted


class Iface(Interface):
    def testThinger(bootz):
        """
        Parameters:
         - bootz
        """
        pass

    def testMe(hello, wonk):
        """
        Parameters:
         - hello
         - wonk
        """
        pass

    def testVoid():
        pass

    def testI32(boo):
        """
        Parameters:
         - boo
        """
        pass


class Client(object):
    implements(Iface)

    def __init__(self, transport, oprot_factory):
        self._transport = transport
        self._oprot_factory = oprot_factory
        self._seqid = 0
        self._reqs = {}

    def testThinger(self, bootz):
        """
        Parameters:
         - bootz
        """
        seqid = self._seqid = self._seqid + 1
        self._reqs[seqid] = defer.Deferred()

        d = defer.maybeDeferred(self.send_testThinger, bootz)
        d.addCallbacks(
            callback=self.cb_send_testThinger,
            callbackArgs=(seqid,),
            errback=self.eb_send_testThinger,
            errbackArgs=(seqid,))
        return d

    def cb_send_testThinger(self, _, seqid):
        return self._reqs[seqid]

    def eb_send_testThinger(self, f, seqid):
        d = self._reqs.pop(seqid)
        d.errback(f)
        return d

    def send_testThinger(self, bootz):
        oprot = self._oprot_factory.getProtocol(self._transport)
        oprot.writeMessageBegin('testThinger', TMessageType.CALL, self._seqid)
        args = testThinger_args()
        args.bootz = bootz
        args.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def recv_testThinger(self, iprot, mtype, rseqid):
        d = self._reqs.pop(rseqid)
        if mtype == TMessageType.EXCEPTION:
            x = TApplicationException()
            x.read(iprot)
            iprot.readMessageEnd()
            return d.errback(x)
        result = testThinger_result()
        result.read(iprot)
        iprot.readMessageEnd()
        if result.success is not None:
            return d.callback(result.success)
        return d.errback(TApplicationException(TApplicationException.MISSING_RESULT, "testThinger failed: unknown result"))

    def testMe(self, hello, wonk):
        """
        Parameters:
         - hello
         - wonk
        """
        seqid = self._seqid = self._seqid + 1
        self._reqs[seqid] = defer.Deferred()

        d = defer.maybeDeferred(self.send_testMe, hello, wonk)
        d.addCallbacks(
            callback=self.cb_send_testMe,
            callbackArgs=(seqid,),
            errback=self.eb_send_testMe,
            errbackArgs=(seqid,))
        return d

    def cb_send_testMe(self, _, seqid):
        return self._reqs[seqid]

    def eb_send_testMe(self, f, seqid):
        d = self._reqs.pop(seqid)
        d.errback(f)
        return d

    def send_testMe(self, hello, wonk):
        oprot = self._oprot_factory.getProtocol(self._transport)
        oprot.writeMessageBegin('testMe', TMessageType.CALL, self._seqid)
        args = testMe_args()
        args.hello = hello
        args.wonk = wonk
        args.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def recv_testMe(self, iprot, mtype, rseqid):
        d = self._reqs.pop(rseqid)
        if mtype == TMessageType.EXCEPTION:
            x = TApplicationException()
            x.read(iprot)
            iprot.readMessageEnd()
            return d.errback(x)
        result = testMe_result()
        result.read(iprot)
        iprot.readMessageEnd()
        if result.success is not None:
            return d.callback(result.success)
        if result.g is not None:
            return d.errback(result.g)
        return d.errback(TApplicationException(TApplicationException.MISSING_RESULT, "testMe failed: unknown result"))

    def testVoid(self):
        seqid = self._seqid = self._seqid + 1
        self._reqs[seqid] = defer.Deferred()

        d = defer.maybeDeferred(self.send_testVoid)
        d.addCallbacks(
            callback=self.cb_send_testVoid,
            callbackArgs=(seqid,),
            errback=self.eb_send_testVoid,
            errbackArgs=(seqid,))
        return d

    def cb_send_testVoid(self, _, seqid):
        return self._reqs[seqid]

    def eb_send_testVoid(self, f, seqid):
        d = self._reqs.pop(seqid)
        d.errback(f)
        return d

    def send_testVoid(self):
        oprot = self._oprot_factory.getProtocol(self._transport)
        oprot.writeMessageBegin('testVoid', TMessageType.CALL, self._seqid)
        args = testVoid_args()
        args.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def recv_testVoid(self, iprot, mtype, rseqid):
        d = self._reqs.pop(rseqid)
        if mtype == TMessageType.EXCEPTION:
            x = TApplicationException()
            x.read(iprot)
            iprot.readMessageEnd()
            return d.errback(x)
        result = testVoid_result()
        result.read(iprot)
        iprot.readMessageEnd()
        if result.g is not None:
            return d.errback(result.g)
        return d.callback(None)

    def testI32(self, boo):
        """
        Parameters:
         - boo
        """
        seqid = self._seqid = self._seqid + 1
        self._reqs[seqid] = defer.Deferred()

        d = defer.maybeDeferred(self.send_testI32, boo)
        d.addCallbacks(
            callback=self.cb_send_testI32,
            callbackArgs=(seqid,),
            errback=self.eb_send_testI32,
            errbackArgs=(seqid,))
        return d

    def cb_send_testI32(self, _, seqid):
        return self._reqs[seqid]

    def eb_send_testI32(self, f, seqid):
        d = self._reqs.pop(seqid)
        d.errback(f)
        return d

    def send_testI32(self, boo):
        oprot = self._oprot_factory.getProtocol(self._transport)
        oprot.writeMessageBegin('testI32', TMessageType.CALL, self._seqid)
        args = testI32_args()
        args.boo = boo
        args.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def recv_testI32(self, iprot, mtype, rseqid):
        d = self._reqs.pop(rseqid)
        if mtype == TMessageType.EXCEPTION:
            x = TApplicationException()
            x.read(iprot)
            iprot.readMessageEnd()
            return d.errback(x)
        result = testI32_result()
        result.read(iprot)
        iprot.readMessageEnd()
        if result.success is not None:
            return d.callback(result.success)
        return d.errback(TApplicationException(TApplicationException.MISSING_RESULT, "testI32 failed: unknown result"))


class Processor(TProcessor):
    implements(Iface)

    def __init__(self, handler):
        self._handler = Iface(handler)
        self._processMap = {}
        self._processMap["testThinger"] = Processor.process_testThinger
        self._processMap["testMe"] = Processor.process_testMe
        self._processMap["testVoid"] = Processor.process_testVoid
        self._processMap["testI32"] = Processor.process_testI32

    def process(self, iprot, oprot):
        (name, type, seqid) = iprot.readMessageBegin()
        if name not in self._processMap:
            iprot.skip(TType.STRUCT)
            iprot.readMessageEnd()
            x = TApplicationException(TApplicationException.UNKNOWN_METHOD, 'Unknown function %s' % (name))
            oprot.writeMessageBegin(name, TMessageType.EXCEPTION, seqid)
            x.write(oprot)
            oprot.writeMessageEnd()
            oprot.trans.flush()
            return defer.succeed(None)
        else:
            return self._processMap[name](self, seqid, iprot, oprot)

    def process_testThinger(self, seqid, iprot, oprot):
        args = testThinger_args()
        args.read(iprot)
        iprot.readMessageEnd()
        result = testThinger_result()
        d = defer.maybeDeferred(self._handler.testThinger, args.bootz)
        d.addCallback(self.write_results_success_testThinger, result, seqid, oprot)
        return d

    def write_results_success_testThinger(self, success, result, seqid, oprot):
        result.success = success
        oprot.writeMessageBegin("testThinger", TMessageType.REPLY, seqid)
        result.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def process_testMe(self, seqid, iprot, oprot):
        args = testMe_args()
        args.read(iprot)
        iprot.readMessageEnd()
        result = testMe_result()
        d = defer.maybeDeferred(self._handler.testMe, args.hello, args.wonk)
        d.addCallback(self.write_results_success_testMe, result, seqid, oprot)
        d.addErrback(self.write_results_exception_testMe, result, seqid, oprot)
        return d

    def write_results_success_testMe(self, success, result, seqid, oprot):
        result.success = success
        oprot.writeMessageBegin("testMe", TMessageType.REPLY, seqid)
        result.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def write_results_exception_testMe(self, error, result, seqid, oprot):
        try:
            error.raiseException()
        except Goodbye as g:
            result.g = g
        oprot.writeMessageBegin("testMe", TMessageType.REPLY, seqid)
        result.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def process_testVoid(self, seqid, iprot, oprot):
        args = testVoid_args()
        args.read(iprot)
        iprot.readMessageEnd()
        result = testVoid_result()
        d = defer.maybeDeferred(self._handler.testVoid, )
        d.addCallback(self.write_results_success_testVoid, result, seqid, oprot)
        d.addErrback(self.write_results_exception_testVoid, result, seqid, oprot)
        return d

    def write_results_success_testVoid(self, success, result, seqid, oprot):
        result.success = success
        oprot.writeMessageBegin("testVoid", TMessageType.REPLY, seqid)
        result.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def write_results_exception_testVoid(self, error, result, seqid, oprot):
        try:
            error.raiseException()
        except Goodbye as g:
            result.g = g
        oprot.writeMessageBegin("testVoid", TMessageType.REPLY, seqid)
        result.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

    def process_testI32(self, seqid, iprot, oprot):
        args = testI32_args()
        args.read(iprot)
        iprot.readMessageEnd()
        result = testI32_result()
        d = defer.maybeDeferred(self._handler.testI32, args.boo)
        d.addCallback(self.write_results_success_testI32, result, seqid, oprot)
        return d

    def write_results_success_testI32(self, success, result, seqid, oprot):
        result.success = success
        oprot.writeMessageBegin("testI32", TMessageType.REPLY, seqid)
        result.write(oprot)
        oprot.writeMessageEnd()
        oprot.trans.flush()

# HELPER FUNCTIONS AND STRUCTURES


class testThinger_args(object):
    """
    Attributes:
     - bootz
    """

    thrift_spec = (
        None,  # 0
        (1, TType.STRING, 'bootz', 'UTF8', None, ),  # 1
    )

    def __init__(self, bootz=None,):
        self.bootz = bootz

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.STRING:
                    self.bootz = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testThinger_args')
        if self.bootz is not None:
            oprot.writeFieldBegin('bootz', TType.STRING, 1)
            oprot.writeString(self.bootz.encode('utf-8') if sys.version_info[0] == 2 else self.bootz)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testThinger_result(object):
    """
    Attributes:
     - success
    """

    thrift_spec = (
        (0, TType.STRING, 'success', 'UTF8', None, ),  # 0
    )

    def __init__(self, success=None,):
        self.success = success

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 0:
                if ftype == TType.STRING:
                    self.success = iprot.readString().decode('utf-8') if sys.version_info[0] == 2 else iprot.readString()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testThinger_result')
        if self.success is not None:
            oprot.writeFieldBegin('success', TType.STRING, 0)
            oprot.writeString(self.success.encode('utf-8') if sys.version_info[0] == 2 else self.success)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testMe_args(object):
    """
    Attributes:
     - hello
     - wonk
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I32, 'hello', None, 64, ),  # 1
        (2, TType.STRUCT, 'wonk', (Hello, Hello.thrift_spec), None, ),  # 2
    )

    def __init__(self, hello=thrift_spec[1][4], wonk=None,):
        self.hello = hello
        self.wonk = wonk

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I32:
                    self.hello = iprot.readI32()
                else:
                    iprot.skip(ftype)
            elif fid == 2:
                if ftype == TType.STRUCT:
                    self.wonk = Hello()
                    self.wonk.read(iprot)
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testMe_args')
        if self.hello is not None:
            oprot.writeFieldBegin('hello', TType.I32, 1)
            oprot.writeI32(self.hello)
            oprot.writeFieldEnd()
        if self.wonk is not None:
            oprot.writeFieldBegin('wonk', TType.STRUCT, 2)
            self.wonk.write(oprot)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testMe_result(object):
    """
    Attributes:
     - success
     - g
    """

    thrift_spec = (
        (0, TType.STRUCT, 'success', (Hello, Hello.thrift_spec), None, ),  # 0
        (1, TType.STRUCT, 'g', (Goodbye, Goodbye.thrift_spec), None, ),  # 1
    )

    def __init__(self, success=None, g=None,):
        self.success = success
        self.g = g

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 0:
                if ftype == TType.STRUCT:
                    self.success = Hello()
                    self.success.read(iprot)
                else:
                    iprot.skip(ftype)
            elif fid == 1:
                if ftype == TType.STRUCT:
                    self.g = Goodbye()
                    self.g.read(iprot)
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testMe_result')
        if self.success is not None:
            oprot.writeFieldBegin('success', TType.STRUCT, 0)
            self.success.write(oprot)
            oprot.writeFieldEnd()
        if self.g is not None:
            oprot.writeFieldBegin('g', TType.STRUCT, 1)
            self.g.write(oprot)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testVoid_args(object):

    thrift_spec = (
    )

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testVoid_args')
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testVoid_result(object):
    """
    Attributes:
     - g
    """

    thrift_spec = (
        None,  # 0
        (1, TType.STRUCT, 'g', (Goodbye, Goodbye.thrift_spec), None, ),  # 1
    )

    def __init__(self, g=None,):
        self.g = g

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.STRUCT:
                    self.g = Goodbye()
                    self.g.read(iprot)
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testVoid_result')
        if self.g is not None:
            oprot.writeFieldBegin('g', TType.STRUCT, 1)
            self.g.write(oprot)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testI32_args(object):
    """
    Attributes:
     - boo
    """

    thrift_spec = (
        None,  # 0
        (1, TType.I32, 'boo', None, None, ),  # 1
    )

    def __init__(self, boo=None,):
        self.boo = boo

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 1:
                if ftype == TType.I32:
                    self.boo = iprot.readI32()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testI32_args')
        if self.boo is not None:
            oprot.writeFieldBegin('boo', TType.I32, 1)
            oprot.writeI32(self.boo)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)


class testI32_result(object):
    """
    Attributes:
     - success
    """

    thrift_spec = (
        (0, TType.I32, 'success', None, None, ),  # 0
    )

    def __init__(self, success=None,):
        self.success = success

    def read(self, iprot):
        if iprot._fast_decode is not None and isinstance(iprot.trans, TTransport.CReadableTransport) and self.thrift_spec is not None:
            iprot._fast_decode(self, iprot, (self.__class__, self.thrift_spec))
            return
        iprot.readStructBegin()
        while True:
            (fname, ftype, fid) = iprot.readFieldBegin()
            if ftype == TType.STOP:
                break
            if fid == 0:
                if ftype == TType.I32:
                    self.success = iprot.readI32()
                else:
                    iprot.skip(ftype)
            else:
                iprot.skip(ftype)
            iprot.readFieldEnd()
        iprot.readStructEnd()

    def write(self, oprot):
        if oprot._fast_encode is not None and self.thrift_spec is not None:
            oprot.trans.write(oprot._fast_encode(self, (self.__class__, self.thrift_spec)))
            return
        oprot.writeStructBegin('testI32_result')
        if self.success is not None:
            oprot.writeFieldBegin('success', TType.I32, 0)
            oprot.writeI32(self.success)
            oprot.writeFieldEnd()
        oprot.writeFieldStop()
        oprot.writeStructEnd()

    def validate(self):
        return

    def __repr__(self):
        L = ['%s=%r' % (key, value)
             for key, value in self.__dict__.items()]
        return '%s(%s)' % (self.__class__.__name__, ', '.join(L))

    def __eq__(self, other):
        return isinstance(other, self.__class__) and self.__dict__ == other.__dict__

    def __ne__(self, other):
        return not (self == other)
