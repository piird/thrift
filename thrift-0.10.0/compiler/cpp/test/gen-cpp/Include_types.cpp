/**
 * Autogenerated by Thrift Compiler (0.10.0)
 *
 * DO NOT EDIT UNLESS YOU ARE SURE THAT YOU KNOW WHAT YOU ARE DOING
 *  @generated
 */
#include "Include_types.h"

#include <algorithm>
#include <ostream>

#include <thrift/TToString.h>




IncludeTest::~IncludeTest() throw() {
}


void IncludeTest::__set_bools(const  ::thrift::test::Bools& val) {
  this->bools = val;
}

uint32_t IncludeTest::read(::apache::thrift::protocol::TProtocol* iprot) {

  apache::thrift::protocol::TInputRecursionTracker tracker(*iprot);
  uint32_t xfer = 0;
  std::string fname;
  ::apache::thrift::protocol::TType ftype;
  int16_t fid;

  xfer += iprot->readStructBegin(fname);

  using ::apache::thrift::protocol::TProtocolException;

  bool isset_bools = false;

  while (true)
  {
    xfer += iprot->readFieldBegin(fname, ftype, fid);
    if (ftype == ::apache::thrift::protocol::T_STOP) {
      break;
    }
    switch (fid)
    {
      case 1:
        if (ftype == ::apache::thrift::protocol::T_STRUCT) {
          xfer += this->bools.read(iprot);
          isset_bools = true;
        } else {
          xfer += iprot->skip(ftype);
        }
        break;
      default:
        xfer += iprot->skip(ftype);
        break;
    }
    xfer += iprot->readFieldEnd();
  }

  xfer += iprot->readStructEnd();

  if (!isset_bools)
    throw TProtocolException(TProtocolException::INVALID_DATA);
  return xfer;
}

uint32_t IncludeTest::write(::apache::thrift::protocol::TProtocol* oprot) const {
  uint32_t xfer = 0;
  apache::thrift::protocol::TOutputRecursionTracker tracker(*oprot);
  xfer += oprot->writeStructBegin("IncludeTest");

  xfer += oprot->writeFieldBegin("bools", ::apache::thrift::protocol::T_STRUCT, 1);
  xfer += this->bools.write(oprot);
  xfer += oprot->writeFieldEnd();

  xfer += oprot->writeFieldStop();
  xfer += oprot->writeStructEnd();
  return xfer;
}

void swap(IncludeTest &a, IncludeTest &b) {
  using ::std::swap;
  swap(a.bools, b.bools);
}

IncludeTest::IncludeTest(const IncludeTest& other0) {
  bools = other0.bools;
}
IncludeTest& IncludeTest::operator=(const IncludeTest& other1) {
  bools = other1.bools;
  return *this;
}
void IncludeTest::printTo(std::ostream& out) const {
  using ::apache::thrift::to_string;
  out << "IncludeTest(";
  out << "bools=" << to_string(bools);
  out << ")";
}


